<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Invoice extends Model
{
    use HasFactory, HasUuids;

    protected $fillable = [
        'name_customer',
        'category_id',
        'unit',
        'fruit_id',
        'price',
        'quantity',
        'amount',
        'customer_id'
    ];

    protected function casts(): array
    {
        return [
            'price' => 'double',
            'quantity' => 'int',
            'amount' => 'double',
        ];
    }

    public function customer(): BelongsTo
    {
        return $this->belongsTo(Customer::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function fruit()
    {
        return $this->belongsTo(Fruit::class);
    }
}
