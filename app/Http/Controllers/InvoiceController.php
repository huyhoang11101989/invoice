<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Invoice;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use DB;

class InvoiceController extends Controller
{
    public function index()
    {
        $data = Invoice::all();

        return response()->json([
            'data' => $data,
        ], 200);
    }

    public function detail(Request $request)
    {
        $data = Invoice::with('category', 'fruit')->where('customer_id', $request->id)->get();

        return response()->json([
            'data' => $data,
        ], 200);
    }

    public function edit(Request $request)
    {
        $data = Invoice::with('category', 'fruit')->where('id', $request->id)->get();

        return response()->json([
            'data' => $data,
        ], 200);
    }

    public function register(Request $request)
    {
        $data = $request->invoice;

        $invoice = Invoice::create([
            'name_customer' => $data['name'],
            'unit' => $data['unit'],
            'quantity' => $data['quantity'],
            'price' => $data['price'],
            'amount' => $data['amount'],
            'category_id' => $data['category_id'],
            'fruit_id' => $data['fruit_id'],
            'customer_id' => $data['customer_id'],
        ]);

        return response()->json([
            'data' => $invoice,
        ], 200);
    }

    public function update(Request $request)
    {
        $data = $request->invoice;
        $invoice = Invoice::find($data['id']);

        if (isset($invoice)) {
            $invoice->name_customer = $data['name'];
            $invoice->unit = $data['unit'];
            $invoice->quantity = $data['quantity'];
            $invoice->price = $data['price'];
            $invoice->amount = $data['amount'];
            $invoice->category_id = $data['category_id'];
            $invoice->fruit_id = $data['fruit_id'];
            $invoice->customer_id = $data['customer_id'];
            $invoice->save();
        }

        return response()->json([
            'data' => $invoice,
        ], 200);
    }

    public function delete(Request $request)
    {
        $customer_id = $request->id;
        $invoice = Invoice::where('customer_id',$request->id)->delete();

        return response()->json([
            'data' => 'success',
        ], 200);
    }
}
