<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Customer;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class CustomerController extends Controller
{
    public function index()
    {
        $data = Customer::paginate(3);

        return response()->json([
            'data' => $data,
        ], 200);
    }

    public function all()
    {
        $data = Customer::all();

        return response()->json([
            'data' => $data,
        ], 200);
    }

    public function register(Request $request)
    {
        $data = Customer::create([
            'name_customer' => $request->name_customer,
        ]);

        return response()->json([
            'data' => $data,
        ], 200);
    }
}
