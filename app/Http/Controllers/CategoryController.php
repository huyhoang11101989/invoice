<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class CategoryController extends Controller
{
    public function index()
    {
        $data = Category::paginate(3);

        return response()->json([
            'data' => $data,
        ], 200);
    }

    public function all()
    {
        $data = Category::all();

        return response()->json([
            'data' => $data,
        ], 200);
    }

    public function register(Request $request)
    {
        $category = Category::create([
            'name' => $request->name,
        ]);

        return response()->json([
            'data' => $category,
        ], 200);
    }
}
