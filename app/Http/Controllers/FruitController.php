<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Fruit;
use App\Models\Category;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class FruitController extends Controller
{
    public function index()
    {
        $data = Fruit::with('category')->paginate(3);

        return response()->json([
            'data' => $data,
        ], 200);
    }

    public function all()
    {
        $data = Fruit::all();

        return response()->json([
            'data' => $data,
        ], 200);
    }

    public function register(Request $request)
    {
        $data = $request->fruit;
        $fruit = Fruit::create([
            'name' => $data['name'],
            'unit' => $data['unit'],
            'category_id' => $data['category_id'],
        ]);

        return response()->json([
            'data' => $fruit,
        ], 200);
    }

    public function getFruit(Request $request)
    {
        $data = Category::where('id',$request->id)->with('fruit')->get();

        return response()->json([
            'data' => $data,
        ], 200);
    }
}
