<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::dropIfExists('invoices');
        Schema::create('invoices', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('name_customer');
            $table->string('unit', length: 4);
            $table->foreignUuid('category_id')->references('id')->on('categories');
            $table->foreignUuid('fruit_id')->references('id')->on('fruits');
            $table->foreignUuid('customer_id')->references('id')->on('customers');
            $table->double('price');
            $table->integer('quantity');
            $table->double('amount');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('invoices');
    }
};
