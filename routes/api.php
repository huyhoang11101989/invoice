<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

// Route::get('/user', function (Request $request) {
//     return $request->user();
// })->middleware('auth:api');


Route::post('/login', [\App\Http\Controllers\LoginController::class, 'login']);
Route::post('/register', [\App\Http\Controllers\RegisterController::class, 'register']);

Route::middleware('auth:api')->get('/all', [\App\Http\Controllers\UserController::class, 'index']);
Route::post('/first', [\App\Http\Controllers\UserController::class, 'first']);
Route::get('/first', [\App\Http\Controllers\UserController::class, 'first']);

Route::get('/category', [\App\Http\Controllers\CategoryController::class, 'index']);
Route::get('/category/all', [\App\Http\Controllers\CategoryController::class, 'all']);
Route::post('/category', [\App\Http\Controllers\CategoryController::class, 'register']);

Route::get('/fruit', [\App\Http\Controllers\FruitController::class, 'index']);
Route::get('/fruit/all', [\App\Http\Controllers\FruitController::class, 'all']);
Route::post('/fruit', [\App\Http\Controllers\FruitController::class, 'register']);
Route::post('/fruit/id', [\App\Http\Controllers\FruitController::class, 'getFruit']);

Route::get('/invoice', [\App\Http\Controllers\InvoiceController::class, 'index']);
Route::post('/invoice', [\App\Http\Controllers\InvoiceController::class, 'register']);
Route::post('/invoice/detail', [\App\Http\Controllers\InvoiceController::class, 'detail']);
Route::post('/invoice/edit', [\App\Http\Controllers\InvoiceController::class, 'edit']);
Route::post('/invoice/update', [\App\Http\Controllers\InvoiceController::class, 'update']);
Route::post('/invoice/delete', [\App\Http\Controllers\InvoiceController::class, 'delete']);

Route::get('/customer', [\App\Http\Controllers\CustomerController::class, 'index']);
Route::get('/customer/all', [\App\Http\Controllers\CustomerController::class, 'all']);
Route::post('/customer', [\App\Http\Controllers\CustomerController::class, 'register']);