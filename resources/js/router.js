import { createRouter, createWebHistory } from "vue-router";
import Home from "./component/Home.vue";
import Login from "./component/Login.vue";
import Register from "./component/Register.vue";
import NotFound from "./component/NotFound.vue";
import Category from "./component/Category.vue";
import CategoryCreate from "./component/CategoryCreate.vue";
import Fruit from "./component/Fruit.vue";
import FruitCreate from "./component/FruitCreate.vue";
import Invoice from "./component/Invoice.vue";
import InvoiceCreate from "./component/InvoiceCreate.vue";
import InvoiceDetail from "./component/InvoiceDetail.vue";
import InvoiceEdit from "./component/InvoiceEdit.vue";
import Customer from "./component/Customer.vue";
import CustomerCreate from "./component/CustomerCreate.vue";

const routes = [
    { 
        path: "/", 
        component: Home,
        name: 'home'
    },
    { 
        path: "/login", 
        component: Login,
        name: 'login'
    },
    { 
        path: "/register", 
        component: Register,
        name: 'register'
    },
    { 
        path: "/category/create", 
        component: CategoryCreate,
        name: 'categoryCreate'
    },
    { 
        path: "/category", 
        component: Category,
        name: 'category'
    },
    { 
        path: "/fruit", 
        component: Fruit,
        name: 'fruit'
    },
    { 
        path: "/fruit/create", 
        component: FruitCreate,
        name: 'FruitCreate'
    },
    { 
        path: "/invoice", 
        component: Invoice,
        name: 'invoice'
    },
    { 
        path: "/invoice/create", 
        component: InvoiceCreate,
        name: 'invoiceCreate'
    },
    { 
        path: "/customer/create", 
        component: CustomerCreate,
        name: 'customerCreate'
    },
    { 
        path: "/customer", 
        component: Customer,
        name: 'customer'
    },
    { 
        path: "/invoice/:id", 
        component: InvoiceDetail,
        name: 'InvoiceDetail'
    },
    { 
        path: "/invoice/edit/:id", 
        component: InvoiceEdit,
        name: 'InvoiceEdit'
    },
    { 
        path: '/:pathMatch(.*)*',
        component: NotFound,
        name: 'notfound',
    }
];


const router = createRouter({
    history: createWebHistory(),
    routes,
});

export default router
