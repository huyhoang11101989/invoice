import './bootstrap';
import { createApp } from 'vue';
import vuex from 'vuex';
import { createStore } from 'vuex';
import router from "./router";


import App from "./component/App.vue";

const store = createStore({
    state() {
        return {
            counter: 1
        }
    },
    mutations: {}
})

const app = createApp(App)
app.use(router)
app.use(store)
app.mount('#app');